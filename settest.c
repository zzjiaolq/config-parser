
#include "settings.h"
#include <stddef.h>
#include <assert.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
	const char* fname = "config.conf";
	FILE * fp = NULL;
	fp = fopen(fname, "r");
	void * value = NULL;
	int ret = getVariable(fp, "mqttbroker", "mqtt_version", &value);
	if ((ret == 0) && (value != NULL) ) printf("mqtt_version : %s\n", (char*)value);

	assert( fp != NULL );
	if (fp == NULL)
		exit(EXIT_FAILURE);

	fclose(fp);
	return 0;
}