#include "settings.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <regex.h>

int is_comment_line(char* line)
{
	int cflags = REG_EXTENDED | REG_ICASE;
	regex_t reg;
	const char* pattern = "^\\s*#+\\w*\\s*$";
	char *lineptr = NULL;
	int result = -1;

	char errbuf[1024];

	regmatch_t pmatch[1];
	const size_t nmatch = 1;

	int regexInit = regcomp(&reg, pattern, cflags);

	if ( regexInit )
	{
		regerror(regexInit, &reg, errbuf, sizeof(errbuf));
		printf("err: %s\n", errbuf);
		printf("Compile regex failed\n");

	} else {
		int reti = regexec(&reg, line, nmatch, pmatch, 0);
		if ( REG_NOERROR == reti )
		{
			result = 0;
		}

	}

	regfree(&reg);
	return result;

}
int is_section_line(char* line, char** section_name)
{
	int cflags = REG_EXTENDED | REG_ICASE;
	regex_t reg;
	const char* pattern = "^\\s*\\[(\\w+)\\]\\s*$";
	char *lineptr = NULL;
	int result = -1;

	char errbuf[1024];

	regmatch_t pmatch[3];
	const size_t nmatch = 2;

	int regexInit = regcomp(&reg, pattern, cflags);

	if ( regexInit )
	{
		regerror(regexInit, &reg, errbuf, sizeof(errbuf));
		printf("err: %s\n", errbuf);
		printf("Compile regex failed\n");

	} else {
		int reti = regexec(&reg, line, nmatch, pmatch, 0);
		if ( REG_NOERROR == reti )
		{
			int size = pmatch[1].rm_eo - pmatch[1].rm_so;
			if ((lineptr = (char *)malloc( sizeof( char) * size) + 1) != NULL)
			{
				memset(lineptr, '\0', sizeof( char) * size + 1);
				strncpy(lineptr, line + pmatch[1].rm_so, size);
			}

			*section_name = lineptr;
			result = 0;
		}


	}

	regfree(&reg);
	return result;

}
int is_variable_line( char* line, const char* variable_name, void ** value)
{
	int cflags = REG_EXTENDED | REG_ICASE;
	regex_t reg;
	const char* pattern = "^\\s*(\\w+)\\s*=\\s*([a-zA-Z0-9.\'\"]+)\\s*$";
	char *valueptr = NULL, *variableptr = NULL;
	int result = -1;

	char errbuf[1024];

	regmatch_t pmatch[3];
	const size_t nmatch = 3;

	int regexInit = regcomp(&reg, pattern, cflags);

	if ( regexInit )
	{
		regerror(regexInit, &reg, errbuf, sizeof(errbuf));
		printf("err: %s\n", errbuf);
		printf("Compile regex failed\n");

	} else {
		int reti = regexec(&reg, line, nmatch, pmatch, 0);
		if ( REG_NOERROR == reti )
		{
			int size1 = pmatch[1].rm_eo - pmatch[1].rm_so;
			if ((variableptr = (char *)malloc( sizeof( char) * size1) + 1) != NULL)
			{
				memset(variableptr, '\0', sizeof( char) * size1 + 1);
				strncpy(variableptr, line + pmatch[1].rm_so, size1);
			}

			int size2 = pmatch[2].rm_eo - pmatch[2].rm_so;
			if ((valueptr = (char *)malloc( sizeof( char) * size2) + 1) != NULL)
			{
				memset( valueptr, '\0', sizeof( char) * size2 + 1 );
				strncpy( valueptr, line + pmatch[2].rm_so, size2 );
			}
			// printf(" %s = %s \n", variableptr, valueptr);
			if (strcmp( variableptr, variable_name) == 0) {
				result = 0;
				*value = valueptr;
			}

		}


	}

	regfree(&reg);
	return result;

}

int getVariable(FILE* fp, const char* section_name, const char* variable_name, void** value)
{
	//
	char *line = NULL;
	char *current_section = NULL;
	size_t len = 0;
	int i = 0;
	int in_section = 0;
	void *valueptr = NULL;
	int result = -1;

	while ((getline(&line, &len, fp)) != -1)
	{
		char *section_name_tmp = NULL;
		if (is_comment_line(line) == 0) continue;

		if (is_section_line(line, &section_name_tmp) == 0) {
			current_section = section_name_tmp;

			if ((strcmp(section_name, section_name_tmp) == 0) && in_section == 0 ) in_section = 1;
			else break;
		}
		if ((current_section != NULL) && (strcmp(section_name, current_section) == 0) && (in_section == 1)) {
			is_variable_line(line, variable_name, &valueptr);
			*value = valueptr;
			result = 0;
		}


		// fwrite(line, nread, 1, stdout);
	}
	free(line);

	return result;
}
