#ifndef __SETTINGS_H__
#define __SETTINGS_H__
#include <stdio.h>
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

int getVariable(FILE* fp, const char* section_name, const char* variable_name, void** value);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif
#endif